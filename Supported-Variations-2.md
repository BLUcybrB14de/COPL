
# Addditional COPL Variations

<table>
  <thead>
    <tr>
      <th>License</th>
      <th>Use</th>
      <th>Change</th>
      <th>Distribute</th>
      <th>Noncommercial</th>
      <th>Additional Restrictions</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td><img src="./images/M-COPL Skull.webp" height="32" align="left"> <a href="https://codeberg.org/Coding-Liberation.net/COPL/src/branch/main/COPL-Mutualist-1.0.md">COPL-Mutualist-1.0</a></td>
      <td>Yes</td>
      <td>Yes</td>
      <td>Yes</td>
      <td>Yes</td>
      <td>Use only allowed for non-hierarchical, collectively-managed entities that benefit members and the community without exploitation or profit-making.</td>
    </tr>
    <tr>
      <td><img src="./images/A-COPL Skull.webp" height="32" align="left"> <a href="https://codeberg.org/Coding-Liberation.net/COPL/src/branch/main/COPL-Anarchist-1.0.md">COPL-Anarchist-1.0</a></td>
      <td>Yes</td>
      <td>Yes</td>
      <td>Yes</td>
      <td>Yes</td>
      <td>Use only allowed for autonomous collectives or individual developers. Organizations using the software must respect developer autonomy and not try to control the project.</td>
    </tr>
  </tbody>
</table>