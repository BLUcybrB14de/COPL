                                    THE MUTALIST CURSE OF PROMETHEUS LICENSE
                                                Version 1, 29 July 2023

                           Copyright (C) 2023 The Coding Liberation Front - for a better future

                                                    Preamble

    The [Licensor], i.e the entity that has acquired legitimized legal rights over specific intellectual material applicable to the [Software Works], hereby grants this License. The [Licensee], a legal personality capable of entering into contracts and possessing legal standing, desires to make usage of the [Software Works] as per the terms and conditions detailed herein.

                                                  License Grant

    The [Licensor] hereby grants to the [Licensee], to not being subjected to, discriminated, and exempted, from allowance to utilize, fabricate copies, modify, and spread the [Software Works] within the political territory under the jurisdiction of the [Licensee], subjected to the ensuing terms and conditions:

    Four Essential Freedoms: The [Software Works] shall be distributed adhering to the four vital intellectual property freedoms of [Free Software], which encompass the privilege to utilize, reproduce, modify, and spread the [Software Works], outlined in the [Four Freedoms].

    [Derivative Works] Distribution Under Same License Terms: Any modifications or derivative works of the [Software Works] must be distributed adhering to the exact same terms and conditions as mentioned in this license document, without any modifications thereof, or said protections and permissions granted by [Four Freedoms] are void. Additions to the licensing of the [Software Works] are permitted, granted this document remains preserved.

    Internal [Legal Entity] [Proprietary] [Fork] Exception: The [Licensee] is permitted by the [licensor] to modify and distribute the [Binary Form] exclusively for a non-promotional non-product internal usage within its [Legal Entity], provided that the [Binary Form] remains undistributed to any external individual or organization, note that the output of the [Binary Form] holds no such restriction.

    GNU Free Documentation License ("GFDL") Compatiblity: The [licensee] is hereby granted the right to create and distribute documentation of the Software Works ("Documentation"), under the terms and conditions of the GNU Free Documentation License ("GFDL"), and such [Documentation] shall be considered as part of the [Software Works]. The license granted to the Documentation shall be considered compatible with the GFDL, to the extent that it adheres to the terms, and/or clauses therein, taking precident over the other terms, and/or clauses of the COPL. Considered void, if the Internal [Legal Entity] [Proprietary] [Fork] Exception, is in effect.

    License Termination for Absence of Source Code Availability: In case the [Licensee] spreads the [Software Works] to the general public but fails to make the [Source Code] accessible for viewing and modifying, then the license granted under this License shall automatically become null and void.

    Anti-Capitalist Clause: This license is intended for use by individuals, cooperatives, and non-profit organizations, and prohibits the use of the [Software Works] for any purpose that is primarily intended for or directed toward commercial advantage or private monetary compensation.

      The [Licensor] reserves the right to revoke this license for any use of the [Software Works] that violates this clause. For the purposes of this license, [commercial advantage] refers to any use of the [Software Works] that results in monetary gain or other tangible benefits to a [Legal Entity] or individual, including but not limited to the sale, licensing, or distribution of the [Software Works] for profit. [Private monetary compensation] refers to any compensation received by a [Legal Entity] or individual in exchange for the use of the [Software Works], including but not limited to consulting fees, royalties, or other forms of payment. Any [Derivative Works] or [Forks] created from the [Software Works] must be distributed under the terms and conditions of this license, without any modification thereof, and should adhere to the [Four Freedoms]. The [Licensor] retains all rights to the [Branding] associated with the [Software Works] and may prohibit its use by any [Legal Entity] or individual that violates this clause.

      Mutualist Clause: This license variation mandates that any [Legal Entity] using the [Software Works] must contribute to the [Mutual exchange] of goods and services, as well as the creation of a society based on [Voluntary associations] and the [Mutual exchange] of goods and services. Specifically, any [Legal Entity] using the [Software Works] must demonstrate a commitment to [Mutual aid and support], and must prioritize the [Community needs] over individual gain or profit.

    The [Licensor] retains all rights to the [Branding] associated with the [Software Works] and may prohibit its use by any [Legal Entity] or individual that violates this clause.

    Enforcement, Remedies, and Dispute Resolution:

    Enforcement: The [Licensor] reserves the right to take any action necessary to enforce the terms and conditions of this license. Such actions may include, but are not limited to, legal action, injunctive relief, and termination of the license granted under this license.

    Remedies: In the event of a breach of the license terms, the [Licensor] shall be entitled to seek injunctive relief, damages, and any other remedies available under applicable law. The [Licensor] may also seek to recover any reasonable attorneys' fees and costs incurred in enforcing the terms of this license.

    Dispute Resolution: Any dispute arising from or related to this license shall be resolved through arbitration in accordance with the rules of the American Arbitration Association. The arbitration shall take place in the jurisdiction in which the [Licensor] is located, and the arbitrator's decision shall be final and binding on both parties. The parties shall bear their own costs and attorneys' fees associated with the arbitration.

    Indemnification: The [Licensee] agrees to indemnify, defend, and hold harmless the [Licensor], its affiliates, and their respective officers, directors, employees, and agents from and against any and all third party claims, damages, liabilities, costs, and expenses, including reasonable attorneys' fees, arising from or related to:

    (i) the use, distribution, modification, or creation of [Derivative Works] of the [Software Works] by the [Licensee] and [Legal Entity] or its agents, or
    (ii) the [Licensee]'s gross negligence, willful misconduct or breach of this Agreement.

    The [Licensor] shall indemnify, defend, and hold harmless the [Licensee] from and against any and all third party claims, damages, liabilities, costs, and expenses, including reasonable attorneys' fees, arising from or related to the [Licensor]'s gross negligence, willful misconduct or material breach of this Agreement.

    Liability: To the extent permitted by applicable law, the [Licensor] shall not be liable to the [Licensee] or any third party for any indirect, special, incidental or consequential damages arising from or related to the use, distribution, modification, or creation of [Derivative Works] of the [Software Works]. The [Licensor] shall be liable to the [Licensee] or third parties for direct damages to the extent caused by the [Licensor]'s gross negligence or willful misconduct. The [Licensee] assumes all risks associated with their permitted use, distribution, modification, or creation of [Derivative Works] of the [Software Works].

    Patent License Restriction: The [Licensee] shall not file or prosecute any patent applications regarding modifications or [Derivative Works] created from the [Software Works]. The [Licensee] agrees to grant a nonexclusive, royalty-free, irrevocable patent license to any patents or patent applications it owns or controls that would otherwise prevent the free use, distribution, modification or creation of [Derivative Works] of the [Software Works] under the terms of this License.

    Additional Protections: The [Licensor] shall not be liable for any claims, damages, or other liability arising from or related to the use or distribution of the [Software Works] by the [Licensee]. The [Licensee] assumes all responsibility for ensuring that the [Software Works] are used and distributed in compliance with applicable laws and regulations. The [Licensor] shall have no obligation to defend, indemnify, or hold harmless the [Licensee] from any claims, damages, or other liability arising from the use or distribution of the [Software Works].

    Termination: The [Licensor] reserves the right to terminate this license immediately upon written notice to the [Licensee] in the event of a material breach of the license terms that is not cured within ninety (90) days of receipt of written notice of the breach. For minor breaches, the [Licensee] shall have thirty (30) days to cure the breach. Upon termination of the license for an uncured material breach, the [Licensee] shall immediately cease all use, distribution, modification, and creation of [Derivative Works] of the [Software Works] and shall return or destroy all copies of the [Software Works] in its possession or control.

                                                Definitions:

    [1]"[Source Code]" refers to the human-readable version of the [Software Works] that contains the instructions and programming necessary to create and modify the [Software Works]. Source code includes all programming statements, comments, and other annotations used to create the [Software Works], as well as any associated configuration files, build scripts, and documentation that are necessary to compile or modify the [Software Works]. For the purposes of this license, "Source Code" also encompasses any non-executable representations of the [Software Works] that could be considered source code, including but not limited to machine-readable code or code in a high-level interpreted language.

    [2]"[Binary Form]" refers to any representation of the [Software Works] that is not human-readable and must be executed or interpreted by a computer processor. Binary form includes compiled versions of the [Software Works], as well as any associated libraries, configuration files, or other non-human-readable resources necessary for execution. For the purposes of this license, "Binary Form" includes any non-human-readable representations of the [Software Works] that could be considered binary code, including but not limited to object code or bytecode.

    [3]"[Branding]" signifies any logos, trademarks, trade names, service marks, or other distinguishing marks associated with the [Software Works]. For the purposes of this license, "Branding" includes any associated slogans, taglines, or other marketing materials as well.

    [4]"[Distribution]" refers to the act of transferring or making available the [Software Works] in any form, whether in [Source Code] or Binary Form, to any third party. This includes but is not limited to distributing via physical media, electronic transfer, or cloud-based services.

    [5]"[Object Code]" signifies the version of the [Software Works] that is compiled for execution on a computer processor. Object code includes any machine-readable representations of the [Software Works], as well as any associated libraries, configuration files, or other non-human-readable resources necessary for execution. For the purposes of this license, "Object Code" includes any non-human-readable representations of the [Software Works] that could be considered object code, including but not limited to machine code or bytecode.

    [6]"[Documentation]" refers to any written or graphic materials that describe or explain the [Software Works], including user manuals, installation guides, online help, and other forms of documentation. For the purposes of this license, "Documentation" is considered part of the [Software Works] and falls under the same licensing terms and conditions as the rest of the [Software Works].

    "[Software Works]" refers to all works that could match definitions [1-6], licensed under this License, incorporating any modifications or [Derivative Works] works thereof.

    "[Free Software]" refers to [Software Works] that meet the [Four Freedoms] of free software. These freedoms include the ability to use, study, modify, and distribute the [Software Works] without any restrictions or limitations. This includes the freedom to redistribute copies of the [Software Works] and to distribute modified versions of the [Software Works]. For the purposes of this license, "Free Software" encompasses any [Software Works] that meet the principles of free software as defined in this license, and which are made available under a license that ensures the [Four Freedoms]

    "[Four Freedoms]" signifies the set of freedoms that specify free software, which incorporate:

    a. The freedom to utilize the [Software Works]for any purpose.
    b. The freedom to study how the [Software Works] and to modify it.
    c. The freedom to redistribute copies of the [Software Works]
    d. The freedom to distribute modified versions of the [Software Works]

    "[Proprietary]" refers to [Software Works] that are not licensed with the [Four Freedoms], therefore not [Free Software] and are instead owned by a specific [Legal Entity]. Proprietary [Software Works] are typically subject to restrictions on their use, modification, and [Distribution], and are often protected by intellectual property laws or other legal mechanisms. The [Legal Entity] that owns the proprietary [Software Works] has the exclusive right to control their use, modification, and [Distribution], and may impose various conditions or limitations on their use and distribution. Proprietary [Software Works] may be made available to users under various licensing schemes, which may include end-user license agreements or other legal agreements that restrict the user's rights to use, modify, or redistribute the [Software Works]. For the purposes of this License, the term "Proprietary" encompasses any [Software Works] that are not freely available for utilization, modification.

    "[Proprietary Software Works]" refers to the [Software Works] that do not meet the [Four Freedoms] or the definition of [Free Software], and are examples of [Proprietary] [Software Works].

    "[Legal Entity]" refers to any person, business, organization, or other entity that is recognized by law as an independent and separate legal entity with its own rights and obligations. This may include, but is not limited to, corporations, partnerships, limited liability companies, governmental bodies, non-profit organizations, and individuals. A Legal Entity is capable of entering into contracts, owning property, and engaging in legal proceedings, and is considered to be a distinct legal person with its own legal identity and liability separate from its owners, members, or shareholders. For the purposes of this License, the term "Legal Entity" encompasses any entity that is recognized as having legal standing and can enter into contracts, as determined by the applicable laws of the jurisdiction in which the Legal Entity is formed or operates.

    [Fork] refers to a copy of the [Software Works] that has been modified or adapted to create a new version of the [Software Works]. A [Fork] may be created by any party that has the legal right to modify or adapt the [Software Works] and may result in the creation of a new, separate project. The [Licensee] is permitted to create a [Fork] of the [Software Works] provided that the new version is not distributed to any external individual or organization. The internal usage of the [Fork] within the [Legal Entity] is allowed under the license.

    [Derivative Works] refers to any modifications or adaptations of the [Software Works], including a [Fork] of the [Software Works] that result in a new and distinct work. This may include changes to the code, addition of new code, or incorporation of other works into the [Software Works]. Any [Derivative Works] created from the [Software Works] must be distributed under the terms and conditions of this license, without any modification thereof, and should adhere to the [Four Freedoms]. The [Licensee] is allowed to distribute the [Derivative Works] to the general public, provided they are distributed under the same license terms as the original [Software Works]. Any additions to the licensing of the [Software Works] are allowed, provided this document remains preserved.

    "[Individuals]" refer to natural persons who are not acting on behalf of any legal entity, and who use the [Software Works] for non-commercial purposes.

    "[Cooperatives]" refer to organizations that are owned and democratically controlled by their members, and whose purpose is to provide goods or services to their members, rather than to generate profit for a small group of investors or owners.

    "[Non-profit organizations]" refer to entities that are organized for a specific purpose or mission, and whose primary objective is not to generate profit for owners or shareholders, but rather to provide a public benefit or to advance a specific cause or mission. This includes charitable organizations, educational institutions, and other organizations that are recognized as tax-exempt under applicable laws.

    "[Commercial advantage]" refers to any use of the [Software Works] that is intended to generate monetary gain or other tangible benefits for a legal entity or individual, including but not limited to the sale, licensing, or distribution of the [Software Works] for profit.

    "[Private monetary compensation]" refers to any compensation received by any [Legal Entity] or individual in exchange for the use of the [Software Works], including but not limited to   consulting fees, royalties, or other forms of payment.

    "[Mutual exchange]" refers to an economic system where individuals and communities trade goods and services for mutual benefit and cooperation, rather than through market transactions or currency exchange. This principle is central to mutualism, which emphasizes voluntary cooperation and mutual aid for meeting needs and promoting well-being.

    "[Voluntary associations]" are organizations and groups formed and operated based on voluntary participation and mutual cooperation, rather than coercion or authority. Mutualism considers voluntary associations as a means of empowering individuals and promoting social and economic justice.

    "[Mutual aid]" refers to providing assistance, resources, and support to those in need, based on mutual benefit and cooperation. Mutual aid is a crucial aspect of mutualism for promoting social and economic justice, as well as strengthening communities and fostering mutual trust and solidarity.

    "[Community needs]" refer to the collective needs and interests of a particular community,  encompassing basic necessities like food, shelter, healthcare, and education. Mutualism prioritizes community needs over individual gain or profit, aiming to promote the well-being and flourishing of all members of the community.

    "[Market transactions]" refer to exchanges of goods and services mediated by currency or other forms of exchange value, rather than through direct mutual exchange or gift-giving. Mutualism rejects the use of market transactions for organizing economic activity and prioritizes the mutual exchange of goods and services based on the principles of cooperation, mutual benefit, and social and economic justice.

    Additional Terms: Any terms not cited herein but inconsistent with or contradicting this License, shall not apply to the [Software Works]. To the extent that any term of this License is held to be legally invalid or unenforceable, the other terms of this License shall remain in full force and effect.

    Warranty Disclaimer: The [Software Works] are provided on an 'as is' basis, and no warranties, explicit or implied, including but not limited to merchantability, fitness for a particular purpose and non-infringement, are made. The [Licensor] shall not be liable for any claim, damages, or other liability.

    Governing Law: This License shall be governed and construed as per the laws of the [Licensee]'s jurisdiction, without regards to its conflict of law provisions.

