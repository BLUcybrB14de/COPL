<img style="margin-right: 30px;" src="https://codeberg.org/repo-avatars/12a1a57a4549917a31fb80fbf7054668831bf711e95b7ca830724b06e2dea8e7" height="70" align="left">

# Curse of Prometheus License (COPL) <a href="https://codeberg.org/Coding-Liberation.net/COPL"><img src="https://custom-icon-badges.demolab.com/badge/hosted%20on-codeberg-4793CC.svg?logo=codeberg&logoColor=white" alt="Codeberg badge"></a>

<br>

> <em> I hunted out and stole the secret spring
of fire, and hid it in a fennel stalk,
to teach them every art and skill,
with endless benefit ! <br>
> ... For this offense
I now must pay the penalty: to live
nailed to this rock beneath the open sky ...
</em>

The Curse of Prometheus License(s)(COPL) are copyleft license(s) that grants users the benefits of proprietary re-licensing that are typically only available with proprietary licenses. It strikes a balance between permissive and non-permissive licenses, allowing for the utilization, reproduction, modification, and distribution of the software works.

<hr>

### Features and Benefits

The Curse of Prometheus License(s)(COPL) offers the following features and benefits:

<ul>
  <li>Allows for usage, copying, modification, and distribution of software works</li>
  <li>Provides protection and permission granted by the Four Essential Freedoms</li>
  <li>Allows for modifications or derivative works of software works, as long as they are distributed under the same license terms</li>
  <li>Internal Legal Entity Proprietary Fork Exception: Allows for the modification and distribution of binary form exclusively for non-promotional, non-product internal usage within the legal entity</li>
  <li>License Termination: In case the Licensee spreads the software works to the general public but fails to make the source code accessible for viewing and modifying, then the license granted under this License shall automatically become null and void.</li>
</ul>

Additionally, the license allows for the creation and distribution of documentation under the <a href="https://codeberg.org/Coding-Liberation.net/COPL/src/branch/main/COLICENSE"> GNU Free Documentation License (GFDL).</a>

<div>
  <h2>The "Why"?</h2>
  <p>Inspired by the <a href="https://anticapitalist.software/">ACSL</a> &amp; the <a href="https://codeberg.org/devdevdany/polyform-licenses">polyform-licenses</a></p>
  <ul>
    <li>
      Similar to <a href="https://creativecommons.org">Creative Commons</a> COPL offers a <a href="https://creativecommons.org/licenses/#licenses">suite of licenses</a>
    </li>
    <li>
      we offer a variety of copyleft licenses - although not as many as polyform
      <ul>
        <li>many of the polyform licenses are not appropriate for software and the four Freedoms</li>
      </ul>
    </li>
    <li>
      While the <a href="https://anticapitalist.software/">ACSL</a> prevents commercial exploitation, but not exploitation from relicensing
      <ul>
        <li>We attempt to solve both problems, by offering a conditional re-licensing agreement, that protects individuals, but encourages many distributions of otherwise free software.</li>
      </ul>
    </li>
  </ul>
</div>

<hr>

## Supported Licensing Variations

To ensure that users of the COPL have the appropriate protections - depending on the usage we've drafted 4 licenses for pubic usage

<div style="border: 1px solid #ccc; padding: 10px;">
  <table>
    <thead>
      <tr>
        <th>License</th>
        <th>Use</th>
        <th>Change</th>
        <th>Distribute</th>
        <th>Noncommercial</th>
        <th>Additional Restrictions</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td><img src="./images/COPL Skull.webp" height="32" align="left"> <a href="https://codeberg.org/Coding-Liberation.net/COPL/src/branch/main/COPL-1.0.md">COPL-1.0</a></td>
        <td>Yes</td>
        <td>Yes</td>
        <td>Yes</td>
        <td>No</td>
        <td>None</td>
      </tr>
      <tr>
        <td><img src="./images/AC-COPL Skull.webp" height="32" align="left"> <a href="https://codeberg.org/Coding-Liberation.net/COPL/src/branch/main/COPL-Anti-Capitalist-1.0.md">COPL-Anti-Capitalist-1.0</a></td>
        <td>Yes</td>
        <td>Yes</td>
        <td>Yes</td>
        <td>Yes</td>
        <td>None</td>
      </tr>
    </tbody>
  </table>

<br>

<a href="https://codeberg.org/Coding-Liberation.net/COPL/releases"><img src="https://akselmo.dev/assets/images/getitoncodeberg.svg" width="200" align="right"></a>

  <div align="left">
    <table>
      <thead>
        <th><a href="https://codeberg.org/Coding-Liberation.net/COPL/src/branch/main/Supported-Variations-2.md">Other Variations</a></th>
      </thead>
    </table>
  </div>
</div>

Also, note &mdash; the <a href="https://codeberg.org/Coding-Liberation.net/COPL/src/branch/main/COPL-Anarchist-1.0.md">"Anarchist"</a> and <a href="https://codeberg.org/Coding-Liberation.net/COPL/src/branch/main/COPL-Mutualist-1.0.md">"Mutualist"</a> variants fall under the <a href="https://codeberg.org/Coding-Liberation.net/COPL/src/branch/main/COPL-Anti-Capitalist-1.0.md">"Anti-Capitalist"</a> umbrella <br> and are effectively variants of the <a href="https://codeberg.org/Coding-Liberation.net/COPL/src/branch/main/COPL-Anti-Capitalist-1.0.md">AC-COPL</a>

<hr>

## Using COPL License(s)

To use the COPL license, simply include the license text in your software works' code repository. We also appreciate and encourage individuals to use these swag badges as a disclaimer and to promote our <a href="https://www.gnu.org/philosophy/free-sw.en.html">free/libre</a> software license.


## Badges

<a href="https://codeberg.org/Coding-Liberation.net/COPL"><img src="https://codeberg.org/Coding-Liberation.net/COPL/raw/branch/main/images/COPL-badge.webp" alt="COPL-badge.webp" width="110" height="20"></a>`
<a href="https://codeberg.org/Coding-Liberation.net/COPL"><img src="https://codeberg.org/Coding-Liberation.net/COPL/raw/branch/main/images/AC-COPL-badge.webp" alt="AC-COPL-badge.webp" width="110" height="20"></a>

[COPL](https://codeberg.org/Coding-Liberation.net/COPL/src/branch/main/COPL-1.0.md)
    - ```<a href="https://codeberg.org/Coding-Liberation.net/COPL"><img src="https://codeberg.org/Coding-Liberation.net/COPL/raw/branch/main/images/COPL-badge.webp" alt="COPL-badge.webp" width="110" height="20"></a>```

[AC-COPL](https://codeberg.org/Coding-Liberation.net/COPL/src/branch/main/COPL-Anti-Capitalist-1.0.md) 
    - ```<a href="https://codeberg.org/Coding-Liberation.net/COPL"><img src="https://codeberg.org/Coding-Liberation.net/COPL/raw/branch/main/images/AC-COPL-badge.webp" alt="AC-COPL-badge.webp" width="110" height="20"></a>```

<br>

<hr>

<h3>
 .. What are you waiting for,<br>
<em>love</em> something ?  — <b>CURSE</b> IT !
</h3>

<hr>

# Issues + Questions

- I will only respond to issues through <a href="https://codeberg.org/Codeberg/">Codeberg</a>
  - To report an issue for the configuration, please create <a href="https://docs.codeberg.org/getting-started/issue-tracking-basics">an issue</a>
- By reporting an issue, you are required to adhere to the Full <u><strong><a href="https://codeberg.org/Coding-Liberation/Manifesto/src/branch/main/CONTRIBUTING.md">CONTRIBUTING.md</a></strong></u>
    - This is a fork of the <a href="https://www.debian.org/code_of_conduct">Debian Code of Conduct</a>
      - Also note, the first clause & 3-6 of the <a href="https://www.funtoo.org/Wolf_Pack_Philosophy">Wolf-Pack philosphy</a></u> 
  - If at any point, you discover the solution to an issue, please <a href="https://www.funtoo.org/Wolf_Pack_Philosophy">Howl.</a><br>
- <strong>For Contact:</strong> <br><a href="https://proton.me/"><img src="https://codeberg.org/blucybrb14de/dotfiles/raw/branch/gentoo-hyprland/images/protonmail.webp"  style="vertical-align: middle" height="20" width="20"></a> blucybrb14de@coding-liberation.org

Mirrors

<a href="https://codeberg.org/Coding-Liberation.net/COPL">
  <img src="https://codeberg.org/Codeberg/Design/raw/branch/main/logo/icon/svg/codeberg-logo_icon_blue.svg"
       alt="Codeberg"
       width="32">
</a>
<a href="https://gitlab.com/BLUcybrB14de/COPL">
  <img src="https://about.gitlab.com/images/press/press-kit-icon.svg"
       alt="Gitlab"
       width="32">
</a>

<hr>

## (Example)

<a class="tooltip" href="https://codeberg.org/Coding-Liberation.net" data-content="Coding-Liberation.net">
<img class="ui avatar gt-vm" src="https://codeberg.org/repo-avatars/be28b9fa83042b94028bf1f1b28275eebb5926f957648b5faa5842732d512e20"
title="The Coding Liberation Front - for a better future" width="28" height="28" align="left"/></a>

<strong> LICENSE  &mdash;  Unique content of this project is licensed under the <a href="https://codeberg.org/Coding-Liberation.net/COPL/src/branch/main/LICENSE">COPL</a> &amp; <a href="https://codeberg.org/Coding-Liberation.net/COPL/src/branch/main/COLICENSE">GFDL (GNU Free Documentation License)</a></strong>

<a rel="license" href="https://www.gnu.org/licenses/fdl-1.3.en.html"><img alt="GNU Free Documentation License" style="border-width:0" height="125" src="https://static.wikia.nocookie.net/bttf/images/d/d1/Heckert_GNU_white.png/revision/latest?cb=20070222062917"/>
